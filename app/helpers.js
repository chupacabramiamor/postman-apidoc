module.exports = {
    compile: function (tpl, data) {
      var result = tpl;
      for (var key in data) {
        tpl = tpl.replace(new RegExp('{{\s?'+ key +'\s?}}', 'gi'), data[key]);
      }

      return tpl;
    }
}