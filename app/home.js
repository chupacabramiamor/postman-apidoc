import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import _ from 'lodash';
import VueResource from 'vue-resource';
import MethodsListComponent from './components/MethodsList/component.vue';
import MethodDetailsComponent from './components/MethodDetails/component.vue';
import SearchingComponent from './components/Searching/component.vue';

Vue.use(Vuex);
Vue.use(VueResource);
Vue.use(VueRouter);

var store = new Vuex.Store({
    state: {
        env: null
    },

    mutations: {
        envInit: function(state, values) {
            state.env = values;
        }
    }
});

function handleItems(items, group) {
    var result = [];
    group = group || 'Ungrouped';

    for (var index in items) {
        if (items[index].item) {
            result = result.concat(handleItems(items[index].item, items[index].name));
            continue;
        } else result.push(_.extend(items[index], { group: group }));
    }

    return result;
}

Vue.config.devtools = true;

new Vue({
    el: "#vue-app",

    router: new VueRouter({
        beforeEach: function(to, from, next) {
            console.log(to);
        },
        routes: [
            { path: '/method/:name', component: MethodDetailsComponent, name: 'method' },
            // { path: '*', redirect: '/' },
        ]
    }),

    data: {
        activeMethodItem: {},
        searchKeyword: '',
        methodsList: null,
    },

    store: store,

    components: {
        'methods-list': MethodsListComponent,
        'method-details': MethodDetailsComponent,
        'searching': SearchingComponent,
    },

    mounted: function() {
        this.$on('methodCalled', this.onMethodChangeHandler.bind(this));

        var collectionPromise = this.$http.get('/dist/collection.json');
        var environmentPromise = this.$http.get('/dist/environment.json');

        Promise.all([ collectionPromise, environmentPromise ]).then(function(responses) {
            this.methodsList = handleItems(responses[0].data.item);
            var env = {};

            for (var index in responses[1].data.values) {
                env[responses[1].data.values[index].key] = responses[1].data.values[index].value;
            }

            this.$store.commit('envInit', env);

            if (this.$route.name == 'method') {
                this.$emit('methodCalled', { name: this.$route.params.name });
            }
        }.bind(this));
    },

    methods: {
        onMethodChangeHandler: function(payload) {
            this.activeMethodItem = _.find(this.methodsList, {name: payload.name});
        }
    },

    watch: {
        searchKeyword: function(value) {
            this.activeMethodItem = {};
        }
    }
});