var path = require('path');
var vueLoaderPlugin = require('vue-loader/lib/plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

var conf = {
    development: {
        mode: 'development',
        entry: './app/home.js',
        devtool: 'eval',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'apidoc.bundle.js'
        },
        watch:true,
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.js'
            }
        },
        module: {
            rules: [
              {
                test: /\.vue$/,
                loader: 'vue-loader'
              }
            ]
        },
        plugins: [
            new vueLoaderPlugin()
        ]
    },

    production: {
        mode: 'production',
        entry: './app/home.js',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'apidoc.bundle.js'
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.min.js',
                'vuex$': 'vuex/dist/vuex.js',
                'lodash$': 'lodash/lodash.js'
            }
        },
        module: {
            rules: [
              {
                test: /\.vue$/,
                loader: 'vue-loader'
              }
            ]
        },
        plugins: [
            new vueLoaderPlugin()
        ]
    }
};

module.exports = conf[NODE_ENV];